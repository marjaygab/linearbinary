/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linearbinary;

/**
 *
 * @author User
 */

import java.util.*;

public class LinearBinary {

    public static void main(String[] args) {
        // TODO code application logic here
        Search query = new Search();
        
        int choice;
        String userchoice;
        Boolean iscorrect=false,results;
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to O3A Number Search Engine: ");
        
        
        
        while (!iscorrect) {
            System.out.print("Please choose what method to use (Type 1 for Linear, 2 for Binary, 3 to EXIT): ");
            try{
             choice =  input.nextInt();
             //query.clearScreen();
             switch(choice){
        case 1:
            iscorrect=true;
            results = query.linearsearch();
            System.out.println("Do you want to try again?(Y or N): ");
                userchoice = input.next();
                        if(userchoice.equals("y") || userchoice.equals("Y"))
                        iscorrect=false;
            break;
        case 2:
            iscorrect=true;
            query.toAscending();
            
             results = query.binarySearch();
            System.out.println("Do you want to try again?(Y or N): ");
                userchoice = input.next();
                        if(userchoice.equals("y") || userchoice.equals("Y"))
                        iscorrect=false;
            break;
        case 3:
            iscorrect=true;
            break;
        default:
            iscorrect = false;
             break;
                 }
            }
            catch(InputMismatchException e){
            System.err.println("InputMismatch Error. Please type 1 or 2 of your choice.");
            input.next();
            }
             
        }

        System.out.println("Thank you for using our search engine!");

    }
    
    
    
}

class Search{
    Scanner userinput = new Scanner(System.in);
    int[] numbers = new int[500];
    int[] arranged = new int[500];
    int[] test = {1,2,3,4,5};   
        int usernum;
    long presenttime,pasttime,totaltime;
    
    Double pastdouble,presentdouble,totaltimedouble;

//Constructor to generate random numbers for array numbers\
//When the object is called in the main method of the main class
//this method is executed    
Search(){
   
for (int i = 0; i < numbers.length; i++) {
              numbers[i] = (int)(Math.random() * ((numbers.length - 1) + 1)) + 1;
              arranged[i]=numbers[i];
        }
}

void displayGen(){
    for (int i = 0; i < 500; i++) {
        System.out.print(numbers[i] + ",");
        if(i==49 || i==99 || i==149 ||i==199 ||i==249 ||i==299 ||i==349 ||i==399 ||i==449 ||i==499)
            System.out.println();
    }
    
    System.out.println();
      for (int i = 0; i < 500; i++) {
        System.out.print(arranged[i] + ",");
        if(i==49 || i==99 || i==149 ||i==199 ||i==249 ||i==299 ||i==349 ||i==399 ||i==449 ||i==499)
            System.out.println();
    }
}

//method to return true if the Linear Search Method found something
Boolean linearsearch(){
System.out.println("What number do you want to find?: ");
usernum = userinput.nextInt();
int j=0;
Boolean flag = false;
//Get time
pastdouble = Double.parseDouble(stringParse(System.nanoTime()));
    for (int i=0;i<numbers.length;i++) {
        if(usernum==numbers[i]){
        flag = true;
        j=i;
        //presentstring = String.valueOf(present.getTimeInMillis());
        //Get time 
        presentdouble = Double.parseDouble(stringParse(System.nanoTime()));
        //Subtract Previous obtained time to Current obtained time to get Time Elapsed
        totaltimedouble =  (presentdouble-pastdouble)/1000000000;
        }
    }
    //if flag is not true
    if (!flag) {
        //Get Time
        presentdouble = Double.parseDouble(stringParse(System.nanoTime()));
        //Subtract Previous obtained time to Current obtained time to get Time Elapsed
        totaltimedouble =  (presentdouble-pastdouble)/1000000000;
    }
    //if flag is true
    if (flag) {
        System.out.println("The number you search is in our system at Array[" + j + "]");
        System.out.print("Time Elapsed: ");
        //Print double value of Time Elapsed in 6 decimal places
        System.out.printf("%.6f", totaltimedouble);
        System.out.println(" seconds elapsed");
        
        
    }
    else{
        System.out.println("Sorry. The number you searched is not in our system.");
     System.out.print("Time Elapsed: ");
        System.out.printf("%.6f", totaltimedouble);
        System.out.println(" seconds elapsed");
     // System.out.println(pastdouble + " " + presentdouble);
    }
 return flag;
}

Boolean binarySearch(){

    System.out.print("What number do you want to find?: ");
    usernum = userinput.nextInt();
    int j=0;
    Boolean flag = false;
    pastdouble = Double.parseDouble(stringParse(System.nanoTime()));
    
    int low = 0;
    int high = arranged.length-1;
    int mid;
    while (high>=low) {        
        mid = (low+high)/2;
        if (usernum<arranged[mid]) {
            high=mid-1;
        }
        else if (usernum==arranged[mid]) {
            j=mid;
            flag=true;
            presentdouble = Double.parseDouble(stringParse(System.nanoTime()));
            totaltimedouble= (presentdouble-pastdouble)/1000000000;
            break;
        }
        else{
        low = mid+1;
        }
    }
    
    if (!flag) {
         presentdouble = Double.parseDouble(stringParse(System.nanoTime()));
            totaltimedouble= (presentdouble-pastdouble)/1000000000;
    }
    
     if (flag) {
        System.out.println("The number you search is in our system at Array[" + j + "]");
        System.out.print("Time Elapsed: ");
        //Print double value of Time Elapsed in 6 decimal places
        System.out.printf("%.6f", totaltimedouble);
        System.out.println(" seconds elapsed");
    }
     else{
        System.out.println("Sorry. The number you searched is not in our system.");
     System.out.print("Time Elapsed: ");
        System.out.printf("%.6f", totaltimedouble);
        System.out.println(" seconds elapsed");
     // System.out.println(pastdouble + " " + presentdouble);
    }
    
    
return flag;
}


String stringParse(long value){
return String.valueOf(value);
}

void toAscending(){

//code in arranging numbers[] in ascending order
for (int i = 0; i < arranged.length; i++) {
        int currentMin=arranged[i];
        int currentMinIndex = i;
        for (int j = i+1; j < arranged.length; j++) {
            if (currentMin>arranged[j]) {
                currentMin=arranged[j];
                currentMinIndex=j;
            }
    }
        if (currentMinIndex!=1) {
        arranged[currentMinIndex] = arranged[i];
        arranged[i]=currentMin;
    }
    }
}

void clearScreen(){
    for (int i = 0; i < 1000; i++) {
        System.out.println("\b");
    }

}


}

    



